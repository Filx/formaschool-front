import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.css']
})
export class PermissionComponent implements OnInit {
  @Input() right: { desc: string; value: boolean; }
  @Output() swapEvent = new EventEmitter<{}>();
  options: any[];

  constructor() { }

  ngOnInit(): void {
    this.options = [
      { name: 'Check', value: true },
      { name: '-', value: null },
      { name: 'Cross', value: false }
    ];
  }
}
