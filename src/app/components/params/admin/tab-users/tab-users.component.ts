import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/user/user';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-tab-users',
  templateUrl: './tab-users.component.html',
  styleUrls: ['./tab-users.component.css']
})
export class TabUsersComponent implements OnInit {
  env = environment;
  @Input() users: User[];

  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

  deleteUser = (param: User) => {
    this.userService.deleteUser(param.id).subscribe(() => {
      this.users = this.users.filter(user => user.id != param.id);
    });
  }
  swapAdmin = (user: User) => {
    this.userService.updateAdmin(user.id, user.isAdmin).subscribe();
  }
}
