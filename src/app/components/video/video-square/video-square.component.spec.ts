import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoSquareComponent } from './video-square.component';

describe('VideoSquareComponent', () => {
  let component: VideoSquareComponent;
  let fixture: ComponentFixture<VideoSquareComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VideoSquareComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoSquareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
