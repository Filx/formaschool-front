import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { filter } from 'rxjs/operators';
import { VideoFlux } from 'src/app/services/video/videoFlux';

@Component({
  selector: 'app-video-square',
  templateUrl: './video-square.component.html',
  styleUrls: ['./video-square.component.css']
})
export class VideoSquareComponent implements OnInit {
  @ViewChild('remoteVideo') remoteVideo: ElementRef<HTMLVideoElement>;

  @Input() videoFlux: VideoFlux;

  constructor() { }

  ngOnInit(): void {
    this.videoFlux.remoteStream$
      .pipe(filter(res => !!res))
      .subscribe(stream => this.remoteVideo.nativeElement.srcObject = stream)
  }
}
