import { Component, OnInit } from '@angular/core';
import { UserLocalStorage } from 'src/app/models/user/userLocalStorage';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor(private storage: StorageService) { }

  ngOnInit(): void {
  }

  isAdmin = (): boolean => {
    let userStorage: UserLocalStorage;
    this.storage.subscribe("user", user => userStorage = user);
    return userStorage?.isAdmin;
  }
}
