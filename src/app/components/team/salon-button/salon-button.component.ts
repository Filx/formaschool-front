import { Component, Input, OnInit } from '@angular/core';
import { SalonNameTeam } from 'src/app/models/salon/salonNameTeam';
import { ManageRightsService } from 'src/app/services/manage-rights.service';

@Component({
  selector: 'app-salon-button',
  templateUrl: './salon-button.component.html',
  styleUrls: ['./salon-button.component.css']
})
export class SalonButtonComponent implements OnInit {

  @Input() salon: SalonNameTeam;
  // TODO [Improve] ? on/off state (linked to other buttons and the display)
  /** Displayed salon */
  @Input() salonId: string;

  constructor(private manageRightsService: ManageRightsService) { }

  ngOnInit(): void { }

  hasSeeSalon = (): boolean => {
    return this.manageRightsService.hasSeeSalon(this.salon.id);
  }
}
