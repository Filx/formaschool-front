import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/user/user';
import { UserLocalStorage } from 'src/app/models/user/userLocalStorage';
import { SalonService } from 'src/app/services/salon.service';
import { StorageService } from 'src/app/services/storage.service';
import { TeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-modale-add-salon',
  templateUrl: './modale-add-salon.component.html',
  styleUrls: ['./modale-add-salon.component.css']
})
export class ModaleAddSalonComponent implements OnInit {

  user: UserLocalStorage;
  form: FormGroup;
  displayDialog = false;

  @Input() teamId: string;


  constructor(private fb: FormBuilder,
    private service: SalonService,
    private teamService: TeamService,
    private router: Router,
    private storageService: StorageService) {

    this.form = fb.group({ name: "", desc: "" });
  }

  ngOnInit(): void {
    this.storageService.subscribe("user", user => this.user = user);
  }


  openAddSalon() {
    this.displayDialog = !this.displayDialog;
  }

  addSalon() {
    const data = { ...this.form.value, teamId: this.teamId };
    this.service.addSalon(data).subscribe(salonAdded => {
      this.storageService.store("user", this.user);
      this.teamService.addSalon(salonAdded, this.teamId);
      const URL = `/teamMessages/${this.teamId}/${salonAdded.id}`;
      this.router.navigate([URL]);
    })
    this.displayDialog = false;
    this.form.reset();
  }
}
