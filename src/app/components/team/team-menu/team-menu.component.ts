import { Component, Input, OnInit } from '@angular/core';
import { Member } from 'src/app/models/member/member';
import { SalonNameTeam } from 'src/app/models/salon/salonNameTeam';
import { TeamNamePict } from 'src/app/models/team/teamNamePict';
import { UserLocalStorage } from 'src/app/models/user/userLocalStorage';
import { ManageRightsService } from 'src/app/services/manage-rights.service';
import { SalonService } from 'src/app/services/salon.service';
import { StorageService } from 'src/app/services/storage.service';
import { TeamService } from 'src/app/services/team.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-team-menu',
  templateUrl: './team-menu.component.html',
  styleUrls: ['./team-menu.component.css']
})
export class TeamMenuComponent implements OnInit {
  env = environment;

  @Input() teamId: string;
  @Input() salonId: string;

  team: TeamNamePict;
  salons: SalonNameTeam[];

  constructor(private teamService: TeamService, private salonService: SalonService, private storage: StorageService,
    private manageRightsService: ManageRightsService) { }

  ngOnInit(): void {
    this.teamService.findNamePictById(this.teamId).subscribe(team => this.team = team);
    this.salonService.findAllOfTeam(this.teamId).subscribe(salons => this.salons = salons);
    this.teamService.register(this);
  }

  /** get Member which contain the teamId */
  getMemberOfTeam = (teamId: string): Member => {
    let userStorage: UserLocalStorage;
    this.storage.subscribe("user", user => userStorage = user);
    return userStorage.members.find(member => member.team.id == teamId);
  }
  /** Check if connected user has one role with the rights to create and delete a salon */
  hasCreateDeleteSalon = (): boolean => {
    return this.manageRightsService.hasCreateDeleteSalon(this.teamId);
  }

}
