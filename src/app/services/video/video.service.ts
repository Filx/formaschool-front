import { Injectable } from '@angular/core';
import SockJS from 'sockjs-client';
import { UserNamePict } from 'src/app/models/user/userNamePict';
import { VideoComponent } from 'src/app/pages/home/video/video.component';
import { environment } from 'src/environments/environment';
import * as Stomp from 'stompjs';
import { UserLocalStorage } from '../../models/user/userLocalStorage';
import { Video } from '../../models/video';
import { StorageService } from '../storage.service';
import { VideoFlux } from './videoFlux';

@Injectable({
  providedIn: 'root'
})
export class VideoService {

  private stompClient;
  // private mosaics: { [salonId: string]: VideoComponent } = {};
  private user: UserNamePict;

  private on = false;
  private flux: { [memberId: string]: VideoFlux } = {};

  private comp: VideoComponent;

  constructor(storageService: StorageService) {
    storageService.subscribe("user", (user: UserLocalStorage) => this.user = { id: user.id, firstname: user.firstname, lastname: user.lastname, picture: user.picture });
    this.connect();
  }

  // =========================================================================================

  /** Open the WebSockets connections */
  connect() {
    this.stompClient = Stomp.over(new SockJS(environment.apiUrl + "/wsVideo"));
    this.stompClient.debug = null;
    this.stompClient.connect({}, () => this.stompClient.subscribe('/vid/public', this.onMessageReceived));
  }

  // =========================================================================================

  /** Manage the different "messages" received from the WebSocket */
  private onMessageReceived = (obj: any) => {
    let video: Video = JSON.parse(obj.body);
    console.log(video);

    if (video.user.id == this.user.id) // ignore if i'm the sender
      return;
    if (video.to != undefined && video.to != this.user.id) // ignore if it's for someone else
      return;

    if (!this.on) { // If i'm offline
      // TODO list people in call
      console.log("I'm offline");
      if (video.type == 0) { // List 
        // this.sendInfos();
      }
      return;
    }

    console.log("process", video.type);


    if (this.flux[video.user.id]) {
      if (video.type == 3)
        this.processEndCall(video);

      // ignore already connected
      return;
    }

    if (video.type == 0)
      this.sendInfos();
    else if (video.type == 1)
      this.processInfos(video);
    else if (video.type == 2)
      this.processCall(video);
  }

  /** Init the connection with the user sending its informations */
  private processInfos(video: Video) {
    console.log("receive infos", video.user.firstname);

    this.flux[video.user.id] = new VideoFlux(video);
    this.flux[video.user.id].enableCallAnswer();
    this.comp.addFlux(this.flux[video.user.id]);
    setTimeout(() => this.call(video.user.id), 1500);
  }

  /** Accept the connexion with the user sending its informations */
  private processCall(video: Video) {
    console.log("call", video.user.firstname);

    this.flux[video.user.id] = new VideoFlux(video);
    this.flux[video.user.id].establishMediaCall();
    this.comp.addFlux(this.flux[video.user.id]);
  }

  /** Close the connexion with the user sending its informations */
  private processEndCall(video: Video) {
    console.log("endCall", video.user.firstname);

    this.flux[video.user.id].closeMediaCall();
    delete this.flux[video.user.id];
    this.comp.removeFlux(video.user.id);
  }

  // ================================================================================================
  // WebSocket

  /** Ask all connected user to send their informations */
  private askInfos = () => this.stompClient.send("/app/video.check", {}, JSON.stringify({ user: this.user, type: 0, salonId: "123" }));

  /** Send user and connection informations */
  private sendInfos = () => this.stompClient.send("/app/video.check", {}, JSON.stringify({ peerId: VideoFlux.peerId, user: this.user, type: 1, salonId: "123" }));

  /** Ask one user to accept the initialized connection */
  private call = (userId) => this.stompClient.send("/app/video.check", {}, JSON.stringify({ peerId: VideoFlux.peerId, user: this.user, type: 2, salonId: "123", to: userId }));

  /** Tell all users to end the connection */
  private endCall = () => this.stompClient.send("/app/video.check", {}, JSON.stringify({ peerId: VideoFlux.peerId, user: this.user, type: 3, salonId: "123" }));

  // ================================================================================================

  /** Create a connection with all users already connected */
  start() {
    this.on = true;
    this.askInfos();
  }

  /** Close all the connections */
  stop() {
    this.on = false;
    this.endCall();
  }

  // ================================================================================================

  /** Add the view, it wil be notified when called */
  registerVideoMosaic(comp: VideoComponent) {
    this.comp = comp;
  }
}
