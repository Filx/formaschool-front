import Peer from "peerjs";
import { BehaviorSubject, Subject } from "rxjs";
import { Video } from "src/app/models/video";
import { v4 as uuidv4 } from 'uuid';

export class VideoFlux {
    private static peerJsOptions: Peer.PeerJSOption = { debug: 3, config: { iceServers: [{ urls: ['stun:stun1.l.google.com:19302', 'stun:stun2.l.google.com:19302'] }] } };
    public static peerId;
    public static peer;

    private mediaCall: Peer.MediaConnection;

    private localStreamBs: BehaviorSubject<MediaStream> = new BehaviorSubject(null);
    public localStream$ = this.localStreamBs.asObservable();
    private remoteStreamBs: BehaviorSubject<MediaStream> = new BehaviorSubject(null);
    public remoteStream$ = this.remoteStreamBs.asObservable();

    private isCallStartedBs = new Subject<boolean>();
    public isCallStarted$ = this.isCallStartedBs.asObservable();

    // ================================================================================================

    constructor(public videoDTO: Video) {
        this.initPeer();
    }

    public initPeer() {
        if (!VideoFlux.peer || VideoFlux.peer.disconnected) {
            console.log(">>> init peer <<<");

            try {
                VideoFlux.peerId = uuidv4();
                VideoFlux.peer = new Peer(VideoFlux.peerId, VideoFlux.peerJsOptions);
            } catch (error) {
                console.error(error);
            }
        }
    }

    // ================================================================================================

    /** Accept the incomming connection (data from videoDTO)  */
    public async establishMediaCall() {
        try {
            const stream = await navigator.mediaDevices.getUserMedia({ video: true, audio: true });

            const connection = VideoFlux.peer.connect(this.videoDTO.peerId);
            connection.on('error', err => console.error(err));

            this.mediaCall = VideoFlux.peer.call(this.videoDTO.peerId, stream);
            if (!this.mediaCall)
                throw new Error('Unable to connect to remote peer');

            this.localStreamBs.next(stream);
            this.isCallStartedBs.next(true);

            this.mediaCall.on('stream', (remoteStream) => this.remoteStreamBs.next(remoteStream));
            this.mediaCall.on('error', err => {
                console.error(err);
                this.isCallStartedBs.next(false);
            });
            this.mediaCall.on('close', () => this.onCallClose());
        } catch (ex) {
            console.error(ex);
            this.isCallStartedBs.next(false);
        }
    }

    /** Init the connection (wait for the other to accept it) */
    public async enableCallAnswer() {
        try {
            const stream = await navigator.mediaDevices.getUserMedia({ video: true, audio: true });
            this.localStreamBs.next(stream);

            VideoFlux.peer.on('call', async (call) => {
                this.mediaCall = call;
                this.isCallStartedBs.next(true);

                this.mediaCall.answer(stream);
                this.mediaCall.on('stream', (remoteStream) => this.remoteStreamBs.next(remoteStream));
                this.mediaCall.on('error', err => {
                    this.isCallStartedBs.next(false);
                    console.error(err);
                });
                this.mediaCall.on('close', () => this.onCallClose());
            });
        }
        catch (ex) {
            console.error(ex);
            this.isCallStartedBs.next(false);
        }
    }

    // ================================================================================================

    /** End the call */
    public closeMediaCall() {
        this.mediaCall?.close();
        if (!this.mediaCall)
            this.onCallClose()
        this.isCallStartedBs.next(false);
    }

    private onCallClose() {
        this.remoteStreamBs?.value.getTracks().forEach(track => track.stop());
        this.localStreamBs?.value.getTracks().forEach(track => track.stop());
    }

    public destroyPeer() {
        this.mediaCall?.close();
        VideoFlux.peer?.disconnect();
        VideoFlux.peer?.destroy();
    }
}
