import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { RoleCreate } from '../models/role/roleCreate';
import { Role } from '../models/role/role';
import { RoleWithoutRights } from '../models/role/roleWithoutRights';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  nextId = 10;
  defaultList = [
    { desc: "Créer/Editer salon(déplacer salon)", value: false },
    { desc: "Créer/Editer role", value: false },
    { desc: "Permettre de mentionner", value: false },
    { desc: "Supprimer les messages des autres", value: false }
  ];

  constructor(private http: HttpClient) { }

  findRoleWithoutRightsInTeamWithoutPermission = (salonId): Observable<RoleWithoutRights[]> => {
    return this.http.get<RoleWithoutRights[]>(`${environment.apiUrl}/roles/withoutPermission/${salonId}`);
  }

  findAllWithoutRightsByTeamId = (teamId: string): Observable<RoleWithoutRights[]> => {
    return this.http.get<RoleWithoutRights[]>(`${environment.apiUrl}/roles/withoutRights/${teamId}`);
  }

  /**
   * This function return a Role object with the id you re looking for
   * @param roleId the id of the role you re looking for
   * @returns a Role object
   */
  findRoleById = (roleId: string): Observable<Role> => {
    return this.http.get<Role>(`${environment.apiUrl}/roles/withDesc/${roleId}`);
  }
}