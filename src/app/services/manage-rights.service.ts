import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommonRights } from '../models/commonRights';
import { Member } from '../models/member/member';
import { PermissionStorage } from '../models/permission/permissionStorage';
import { UserLocalStorage } from '../models/user/userLocalStorage';
import { SalonService } from './salon.service';
import { StorageService } from './storage.service';
import { TeamService } from './team.service';

@Injectable({
  providedIn: 'root'
})
export class ManageRightsService {

  constructor(private storage: StorageService, private salonService: SalonService) { }

  /** get Member which contain the teamId */
  getMemberOfTeam = (teamId: string): Member => {
    let userStorage: UserLocalStorage;
    this.storage.subscribe("user", user => userStorage = user);
    return userStorage.members.find(member => member.team.id == teamId);
  }

  /** Check if connected user has the rights to see logs */
  hasLogsRights = (teamId: string): boolean => {
    if (this.isAdmin())
      return true;
    let res: boolean = false;
    let memberOfTeam = this.getMemberOfTeam(teamId);
    memberOfTeam.roles.forEach(role => {
      res = role.seeLogs
    });
    return res;
  }
  /** Check if connected user has one role with the rights to create and delete a salon */
  hasCreateDeleteSalon = (teamId: string): boolean => {
    let res: boolean = false;
    let userStorage: UserLocalStorage;
    this.storage.subscribe("user", user => userStorage = user);
    if (userStorage.isAdmin)
      return true;
    let memberOfTeam = this.getMemberOfTeam(teamId);
    memberOfTeam.roles.forEach(role => {
      res = role.createDeleteSalon ? true : false;
    })
    return res;
  }

  /** Check if connected user has the rights to see logs */
  hasEmojisRights = (teamId: string): boolean => {
    if (this.isAdmin())
      return true;
    let res: boolean = false;
    let memberOfTeam = this.getMemberOfTeam(teamId);
    memberOfTeam.roles.forEach(role => {
      res = role.manageEmoji
    });
    return res;
  }

  /** Complex function, check if connected user has the right for this specific salon to manager permission.
   *  It return an observable because you have to wait the api to respond you about the teamId of your salon.
   */
  hasPermission = (salonId: string, num: number): boolean => {
    let res: boolean = false;
    //if user connected is admin then it return true;
    if (this.isAdmin())
      return true;
    let permissions: PermissionStorage[] = this.getPermission(salonId);

    // If it exist a permission for the specific member, it win compare to a permission for a role
    let permissionMember: PermissionStorage = permissions.find(permission => permission.member != null)
    if (permissionMember != null) {
      res = this.getBoolean(permissionMember.commonRights, num);
      if (res != null)
        return res;
    }

    // If it not exist permission for specific member and exist a permission for a role on this salon
    let permissionRole: PermissionStorage[] = permissions.filter(permissions => permissions.role != null);
    if (permissionMember == null && permissionRole.length != 0) {
      permissionRole.forEach(permission => res = !res ? this.getBoolean(permission.commonRights, num) : true);
      if (res != null)
        return res;
    }

    // If it not exist permission for specific member and role for this salon then look if one role at least got the permission
    if ((permissionRole.length == 0 && permissionMember == null) || res == null) {
      let memberOfTeam = this.getMemberOfTeam(this.salonService.getTeamIdOf(salonId));
      memberOfTeam?.roles.forEach(role => {
        res = !res ? this.getBoolean(role.commonRights, num) : true;
      });
      return res;
    }
    else {
      return res;
    }
  }

  hasManagePermission = (salonId: string): boolean => {
    return this.hasPermission(salonId, 3);
  }
  hasUpdateSalon = (salonId: string): boolean => {
    return this.hasPermission(salonId, 7);
  }
  hasDeleteMsg = (salonId: string): boolean => {
    return this.hasPermission(salonId, 2);
  }
  hasSeeSalon = (salonId: string): boolean => {
    return this.hasPermission(salonId, 4);
  }
  hasSendMsg = (salonId: string): boolean => {
    return this.hasPermission(salonId, 5);
  }
  hasAddReaction = (salonId: string): boolean => {
    return this.hasPermission(salonId, 1);
  }

  /** return the good boolean depending on the number passed */
  getBoolean = (commonRights: CommonRights, num: number): boolean => {
    if (num == 1)
      return commonRights.addReaction;
    if (num == 2)
      return commonRights.deleteMsg;
    if (num == 3)
      return commonRights.managePermissions;
    if (num == 4)
      return commonRights.seeSalon;
    if (num == 5)
      return commonRights.sendMsg;
    if (num == 6)
      return commonRights.tagSomeone;
    return commonRights.updateSalon
  }

  isAdmin = () => {
    let userStorage: UserLocalStorage;
    this.storage.subscribe("user", user => userStorage = user);
    return userStorage.isAdmin;
  }

  private getPermission(salonId: string) {
    let userStorage: UserLocalStorage;
    this.storage.subscribe("user", user => userStorage = user);
    let permissions: PermissionStorage[] = userStorage.permissions.filter(permission => permission.salon.id == salonId);
    return permissions;
  }
}
