import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { UserLocalStorage } from '../models/user/userLocalStorage';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class IsSignInGuard implements CanActivate {

  user: UserLocalStorage;

  constructor(private storageService: StorageService, private router: Router) {

  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    this.storageService.subscribe("user", user => this.user = user);
    if (this.user == null) {
      this.router.navigate(["/login"]);
    }
    return this.user != null;
  }

}
