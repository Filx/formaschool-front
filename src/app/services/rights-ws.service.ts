import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { UserLocalStorage } from '../models/user/userLocalStorage';
import { StorageService } from './storage.service';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { Role } from '../models/role/role';
import { RoleCreate } from '../models/role/roleCreate';


@Injectable({
  providedIn: 'root'
})
export class RightsWsService {

  private stompClient;
  private stompClientDelete;
  private stompClientAdd;
  private user: UserLocalStorage;

  constructor(private storageService: StorageService) {
    storageService.subscribe("user", user => {
      this.user = user
    });
    this.connect();
  }

  // =========================================================================================

  /** Open the WebSockets connections */
  connect() {
    this.stompClient = Stomp.over(new SockJS(environment.apiUrl + "/wsRights"));
    this.stompClient.debug = null;
    this.stompClient.connect({}, () => this.stompClient.subscribe('/rights/public', this.onMessageReceived));

    this.stompClientDelete = Stomp.over(new SockJS(environment.apiUrl + "/wsRights"));
    this.stompClientDelete.debug = null;
    this.stompClientDelete.connect({}, () => this.stompClientDelete.subscribe('/rights/delete', this.onMessageReceivedDelete));

    this.stompClientAdd = Stomp.over(new SockJS(environment.apiUrl + "/wsRights"));
    this.stompClientAdd.debug = null;
    this.stompClientAdd.connect({}, () => this.stompClientAdd.subscribe('/rights/create', this.onMessageReceivedAdd));
  }
  // =========================================================================================
  // Role
  /** Send the edited content with the WebSocket */
  updateRole = (role: Role) => this.stompClient.send("/app/role.update", {}, JSON.stringify(role));

  addRole = (teamId: string, role: RoleCreate) => this.stompClientAdd.send(`/app/role.add/${teamId}`, {}, JSON.stringify(role));

  deleteRole = (teamId: string, roleId: string) => this.stompClientDelete.send(`/app/role.delete/${teamId}/${roleId}`, {}, {});

  // =========================================================================================
  // Messages management

  /** Manage the different "messages" received from the WebSocket */
  private onMessageReceived = (_obj: any) => {
    let obj = JSON.parse(_obj.body);
    if (obj["color"]) {
      this.receiveRole(obj);
    }
    this.storageService.store("user", this.user);
  }

  private receiveRole(obj: any) {
    this.user.members.forEach(member => {
      member.team.roles = member.team.roles?.map(role => role.id == obj.id ? obj : role);
      member.roles = member.roles?.map(role => role.id == obj.id ? obj : role);
    });
  }

  /** Manage the different "messages" received from the WebSocket */
  private onMessageReceivedDelete = (_obj: any) => {
    let obj = JSON.parse(_obj.body);
    if (obj["color"]) {
      this.deleteRoleFromStorage(obj);
    }
    this.storageService.store("user", this.user);
  }

  /** Delete role from localStorage */
  private deleteRoleFromStorage(obj: any) {
    this.user.members.forEach(member => {
      member.team.roles = member.team.roles?.filter(role => role.id != obj.id);
      member.roles = member.roles?.filter(role => role.id != obj.id);
    });
  }

  /** Manage the different "messages" received from the WebSocket */
  private onMessageReceivedAdd = (_obj: any) => {
    let obj = JSON.parse(_obj.body);
    if (obj["color"]) {
      this.addRoleToStorage(obj);
    }
    this.storageService.store("user", this.user);
  }
  /** Delete role from localStorage */
  private addRoleToStorage(obj: any) {
    this.user.members.forEach(member => {
      member.team.roles?.push(obj);
    });
  }
}
