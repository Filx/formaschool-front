import { UserNamePict } from "../user/userNamePict";

export interface Emoji {
    id: string;
    name: string;
    teamId: string;
    user: UserNamePict;
    picture: string;
}