export interface EmojiDesc {
    annotation: string; //alias
    group: string; // team ou orga
    hexcode?: string;
    openmoji_tags?: string;
    order?: number;
    subgroups?: string;
    tags?: string;

    path: string; // emojiTeam/id
    id: string; // id
}
