export interface CommonRights{
    addReaction: boolean;
    deleteMsg: boolean;
    managePermissions: boolean;
    seeSalon: boolean;
    sendMsg: boolean;
    tagSomeone: boolean;
    updateSalon: boolean;
}