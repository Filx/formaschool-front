import { CommonRights } from "../commonRights";
import { Member } from "../member/member";
import { Role } from "../role/role";
import { Salon } from "../salon/salon";

export interface PermissionStorage {
    id: string;
    salon: Salon;
    member: Member;
    role: Role;
    commonRights: CommonRights;
}