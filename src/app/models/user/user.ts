export interface User {
    id: string;
    firstname: string;
    lastname: string;
    isAdmin: boolean;
    email: string;
    phone: string;
    create: Date;
    picture: string;
}
