import { Member } from "../member/member";
import { PermissionStorage } from "../permission/permissionStorage";

export interface UserLocalStorage {
    id: string;
    firstname: string;
    lastname: string;
    picture: string;
    isAdmin: boolean;
    permissions: PermissionStorage[];
    members: Member[];
}
