import { UserNamePict } from "./user/userNamePict";

export interface Video {
    type: number;
    user: UserNamePict;
    peerId?: string;

    salonId: string;
    to: string;
}
