import { CommonRights } from "../commonRights";

export interface RoleStorage {
    id: string;
    color: string;
    name: string;
    changePseudo: boolean;
    createDeleteSalon: boolean;
    manageEmoji: boolean;
    manageTeam: boolean;
    managePseudo: boolean;
    seeLogs: boolean;
    commonRights: CommonRights;
}