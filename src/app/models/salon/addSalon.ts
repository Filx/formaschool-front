export interface AddSalon {
    name: string;
    desc: string;
    teamId: string;
}
