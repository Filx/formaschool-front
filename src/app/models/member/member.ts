import { Role } from "../role/role";
import { RoleStorage } from "../role/roleStorage";
import { Team } from "../team/team";
import { User } from "../user/user";

export interface Member {
    id: string;
    pseudo: string;
    user: User;
    team: Team;
    roles: RoleStorage[];
}
