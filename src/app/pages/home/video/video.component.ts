import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { VideoService } from 'src/app/services/video/video.service';
import { VideoFlux } from 'src/app/services/video/videoFlux';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {

  public isCallStarted$: Observable<boolean>;

  @ViewChild('localVideo') localVideo: ElementRef<HTMLVideoElement>;

  videoFlux: VideoFlux[] = [];

  // ================================================================================================

  constructor(private videoService: VideoService) {
    videoService.registerVideoMosaic(this);
  }

  ngOnInit(): void {
  }

  // ================================================================================================

  startVideo() {
    this.videoService.start();
  }

  endVideo() {
    for (let index in this.videoFlux)
      this.videoFlux[index].closeMediaCall();
    this.videoFlux = [];
    this.localVideo.nativeElement.srcObject = undefined;
    this.videoService.stop();
  }

  // ================================================================================================

  addFlux(flux: VideoFlux) {
    this.videoFlux.push(flux);
    if (this.videoFlux.length == 1) // Start local stream
      this.videoFlux[0].localStream$
        .pipe(filter(res => !!res))
        .subscribe(stream => this.localVideo.nativeElement.srcObject = stream)
  }

  removeFlux(userId: string) {
    this.videoFlux = this.videoFlux.filter(flux => flux.videoDTO.user.id != userId);
    if (this.videoFlux.length == 0)
      this.localVideo.nativeElement.srcObject = undefined;
  }
}
