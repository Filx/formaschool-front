import { AfterContentInit, AfterViewInit, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit, AfterContentInit {
  styleColors = { display: "none" };

  header = "#4D729E";
  menu = "#7295BF";
  background = "#92BADE";
  font_1 = "#A2D0EA";
  font_2 = "#1E346D";

  one = "#1E346D";
  one2 = "#1E346D";
  two = "#1E346D";

  constructor() { }

  ngOnInit(): void {
  }

  // ================================================================================================
  // From CSS

  // ngAfterViewInit() {
  ngAfterContentInit() {
    const elem = getComputedStyle(document.documentElement);

    this.header = this.propertyToHexa(elem, 'header');
    this.menu = this.propertyToHexa(elem, 'menu');
    this.background = this.propertyToHexa(elem, 'background');
    this.font_1 = this.propertyToHexa(elem, 'font_1');
    this.font_2 = this.propertyToHexa(elem, 'font_2');

    this.one = this.propertyToHexa(elem, 'one');
    this.one2 = this.propertyToHexa(elem, 'one2');
    this.two = this.propertyToHexa(elem, 'two');
  }

  RGBToHexa = (rgb: string) => {
    if (rgb.search('#') != -1)
      return rgb.trim();

    let split = rgb.trim().substring(4, rgb.trim().length - 1).split(",");
    return `#${(+split[0]).toString(16)}${(+split[1]).toString(16)}${(+split[2]).toString(16)}`;
  }

  propertyToHexa = (elem, property: string) => this.RGBToHexa(elem.getPropertyValue('--' + property));

  // ================================================================================================
  // To CSS

  /** Change the color of the whole site (colors from ) */
  changeColor = () => {
    document.documentElement.style.setProperty('--_header', this.hexaToRGB(this.header));
    document.documentElement.style.setProperty('--_menu', this.hexaToRGB(this.menu));
    document.documentElement.style.setProperty('--_background', this.hexaToRGB(this.background));
    document.documentElement.style.setProperty('--_font_1', this.hexaToRGB(this.font_1));
    document.documentElement.style.setProperty('--_font_2', this.hexaToRGB(this.font_2));

    document.documentElement.style.setProperty('--one', `rgb(${this.hexaToRGB(this.one)})`);
    document.documentElement.style.setProperty('--one2', `rgb(${this.hexaToRGB(this.one2)})`);
    document.documentElement.style.setProperty('--two', `rgb(${this.hexaToRGB(this.two)})`);
  }

  /** Returns the colors as string: 'R, G, B' */
  hexaToRGB = (hexa: string): string => `${this.extractNumber(hexa, 1)}, ${this.extractNumber(hexa, 3)}, ${this.extractNumber(hexa, 5)}`;

  /** Extract one bit from an ARGB string */
  extractNumber = (str: string, index: number) => parseInt("0x" + str.substr(index, 2));

  // ================================================================================================

  openColors = (event) => this.styleColors = { display: "block" };

  closeColors = () => this.styleColors = { display: "none" };
}
