import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Role } from 'src/app/models/role/role';
import { RoleWithoutRights } from 'src/app/models/role/roleWithoutRights';
import { RoleService } from 'src/app/services/role.service';
import { TeamService } from 'src/app/services/team.service';
import { RightsWsService } from 'src/app/services/rights-ws.service';
import { StorageService } from 'src/app/services/storage.service';
import { UserLocalStorage } from 'src/app/models/user/userLocalStorage';

@Component({
  selector: 'app-team-roles',
  templateUrl: './team-roles.component.html',
  styleUrls: ['./team-roles.component.css']
})
export class TeamRolesComponent implements OnInit {

  roleSelected: Role;
  roles: RoleWithoutRights[];
  teamId: string;

  constructor(private roleService: RoleService, private teamService: TeamService, private activatedRoute: ActivatedRoute,
    private wsRights: RightsWsService, private storageService: StorageService) {
  }

  ngOnInit(): void {
    this.activatedRoute.parent.paramMap.subscribe(params => {
      this.teamId = params.get("teamId");
      this.storageService.subscribe("user", (user: UserLocalStorage) => {
        this.updateFromLocalStorage(user);
      });
    });
  }

  /**
   * This function refresh the list of roles
   */
  refreshRoles = () => {
    this.roleService.findAllWithoutRightsByTeamId(this.teamId).subscribe(roles => {
      this.roles = roles;
    });
  }

  /**
   * This function refresh the page with the role choosen
   * @param id the id of the role choosen
   */
  roleChoosen = (id: string) => {
    this.roleService.findRoleById(id).subscribe(role => {
      this.roleSelected = role;
    });
  }

  /**
   * This function allows you to create a new role
   */
  addNewRole = () => {
    this.wsRights.addRole(this.teamId, { name: "nouveau role", color: "#A2D0EA" });
  }

  /**
   * This function allows us to update a role
   */
  update = () => {
    this.wsRights.updateRole(this.roleSelected);
  }

  /**
   * Change the color of the role
   * @param color color you want to add
   */
  updateColor = (color: string) => {
    this.roleSelected.color = color;
  }

  /**
   * Delete a role 
   * @param role id of the role
   */
  deleteRole = (role: RoleWithoutRights) => {
    this.wsRights.deleteRole(this.teamId, role.id);
  }

  /**
   * This function refresh role from localStorage
   * @param user 
   */
  private updateFromLocalStorage(user: UserLocalStorage) {
    this.roles = user.members.find(member => member.team.id == this.teamId).team.roles;
    if (this.roles && this.roles.length != 0) {
      this.roleChoosen(this.roles[0].id);
    }
  }
}
