import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Member } from 'src/app/models/member/member';
import { TeamNameDescPict } from 'src/app/models/team/teamNameDescPict';
import { UserLocalStorage } from 'src/app/models/user/userLocalStorage';
import { StorageService } from 'src/app/services/storage.service';
import { TeamService } from 'src/app/services/team.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-team-summary',
  templateUrl: './team-summary.component.html',
  styleUrls: ['./team-summary.component.css']
})
export class TeamSummaryComponent implements OnInit {
  env = environment;

  team: TeamNameDescPict;

  teamId: string;

  constructor(
    private service: TeamService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private storage: StorageService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.parent.paramMap.subscribe(params => {
      this.teamId = params.get("teamId");
      this.service.findNamePicDescById(this.teamId).subscribe(team => this.team = team);
    });
  }

  teamUpdate = (teamId: string) => {
    const URL = `/params/team/${teamId}/summaryUpdate`
    this.router.navigate([URL]);
  }

  /** get Member which contain the teamId */
  getMemberOfTeam = (teamId: string): Member => {
    let userStorage: UserLocalStorage;
    this.storage.subscribe("user", user => userStorage = user);
    return userStorage.members.find(member => member.team.id == teamId);
  }

  /** Check if connected user has one role with the rights to change name and description of a team */
  hasManageTeam = (): boolean => {
    let userStorage: UserLocalStorage;
    this.storage.subscribe("user", user => userStorage = user);
    if(userStorage.isAdmin)
      return true;
    let res: boolean = false;
    let memberOfTeam = this.getMemberOfTeam(this.teamId);
    memberOfTeam.roles.forEach(role => {
      res = role.manageTeam ? true : false;
    })
    return res;
  }
}
