import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { ObserveOnOperator } from 'rxjs/internal/operators/observeOn';
import { CommonRights } from 'src/app/models/commonRights';
import { Member } from 'src/app/models/member/member';
import { PermissionStorage } from 'src/app/models/permission/permissionStorage';
import { UserLocalStorage } from 'src/app/models/user/userLocalStorage';
import { ManageRightsService } from 'src/app/services/manage-rights.service';
import { PermissionService } from 'src/app/services/permission.service';
import { StorageService } from 'src/app/services/storage.service';
import { TeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-params-template',
  templateUrl: './params-template.component.html',
  styleUrls: ['./params-template.component.css']
})
export class ParamsTemplateComponent implements OnInit {

  menus = {
    admin: {
      buttons: [
        { link: "createUser", text: "Ajouter un utilisateur" },
        { link: "createTeam", text: "Ajouter une équipe" },
        { link: "addUserToTeam", text: "Ajouter un utilisateur à une équipe" },
        { link: "adminLogs", text: "Logs Admin" },
        { link: "adminEmojis", text: "Emojis Admin" },
      ],
      // TODO [Improve] Redirect to last visited page
      previous: ""
    },
    team: {
      buttons: [
        { link: "summary", text: "Resumé" },
        { link: "roles", text: "Rôles" },
        { link: "members", text: "Membres" }
      ],
      previous: ""
    },
    salon: {
      buttons: [
        { link: "summary", text: "Resumé" }
      ],
      // TODO [Improve] Redirect to actual salon
      previous: "/teamSelect"
    },
    user: {
      buttons: [
        { link: "general", text: "Resumé" },
        { link: "password", text: "Mot de passe" },
      ],
      previous: "/teamSelect"
    }
  }

  buttons;
  previous: string;

  constructor(private router: Router, private manageRightsService: ManageRightsService) { }

  ngOnInit(): void {
    const url = this.router.url.split("/");
    if (url[2] == "team") {
      if (this.manageRightsService.hasLogsRights(url[3])) {
        this.menus[url[2]].buttons.push({ link: "logs", text: "Logs" });
      }
      if (this.manageRightsService.hasEmojisRights(url[3])) {
        this.menus[url[2]].buttons.push({ link: "emojis", text: "Emojis" });
      }
    }
    if (url[2] == "salon") {
      if(this.manageRightsService.hasManagePermission(url[3]))
        this.menus[url[2]].buttons.push({ link: "permissions", text: "Permissions" });
    }

    this.buttons = this.menus[url[2]].buttons;
    this.previous = url[2] == "team" ? `/teamMessages/${url[3]}/redirect` : this.menus[url[2]].previous;
  }
}
