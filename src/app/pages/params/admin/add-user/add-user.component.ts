import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user/user';
import { UserCreation } from 'src/app/models/user/userCreation';
import { UserCreationWithFile } from 'src/app/models/user/userCreationWithFile';
import { StorageService } from 'src/app/services/storage.service';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  users: User[];
  userForm: FormGroup;
  file: File;

  constructor(private fb: FormBuilder, private userService: UserService, private storageService: StorageService, private router: Router) {
    this.userForm = this.fb.group({
      firstname: [''],
      lastname: [''],
      password: [''],
      email: [''],
    })
  }

  ngOnInit(): void {
    this.refreshUsers();
  }
  refreshUsers = () => {
    this.userService.findAll().subscribe(users => {
      this.users = users;
    })
  }

  /**
   * This function allows us to save a user
   */
  save = () => {
    if (this.file != null) {
      this.saveWithFile();
    }
    else {
      let user: UserCreation = this.userForm.value;
      this.userService.save(user).subscribe(user => {
        this.refreshUsers();
      });
    }
  }

  saveWithFile = () => {
    let user: UserCreationWithFile = this.userForm.value;
    user.file = this.file;
    this.userService.saveWithFile(user);
    setTimeout(() => this.refreshUsers(), 200);
  }

  getEvent = (element) => {
    this.file = element.file;
  }
}
